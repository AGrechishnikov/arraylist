
public class arrayList<T> {
    private Object array[] = new Object[10];
    private int size = 0;

    private void resize(int newSize){
        Object temporary[] = new Object[newSize];
        System.arraycopy(array,0,temporary,0,array.length);
        array=temporary;
    }

    public void add(T element){
        if(size() == array.length){
            resize(2*array.length);
        }
        array[size] = element;
        size++;
    }

    public void remove(int index){
        if(index>=0 && index<size && !isEmpty()){
            if(size - 1 < array.length/4 && array.length > 10){
                resize(array.length/2);
            }
            System.arraycopy(array,0,array,0,index);
            System.arraycopy(array,index+1 , array,index,array.length-index - 1 );
            size--;
        }
    }

    public void remove(T element) {
        for(int i=0;i<size;i++){
            if(array[i]==element){
                remove(i);
                break;
            }
        }
    }

    public boolean contains(T element){
        for(int i=0;i<size;i++){
            if((T)array[i] == element){
                return true;
            }
        }
        return false;
    }

    public int size(){
        return this.size;
    }

    public boolean isEmpty(){
        return size == 0;
    }

    public T get(int index){
        if(index>=0 && index<size && !isEmpty()){
            return (T) array[index];
        }
        return null;
    }

    public void clear(){
        Object temporary[] = new Object[10];
        array=temporary;
        size = 0;
    }
}